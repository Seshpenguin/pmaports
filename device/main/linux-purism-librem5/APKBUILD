# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Alistair Francis <alistair@alistair23.me>
# Co-Maintainer: Newbyte <newbyte@postmarketos.org>
pkgname=linux-purism-librem5
pkgver=6.2.8
pkgrel=0
# NOTE: Don't forget to rebase the config! See prepare() for instructions.
_purismrel=1
# <kernel ver>.<purism kernel release>
_purismver=${pkgver}pureos$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="
	bash
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
install="$pkgname.post-upgrade"

# Source
_repository="linux"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"


source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/linux/-/archive/pureos/$_purismver/linux-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare

	####### Rebase config
	# The kernel config needs to be periodically rebased to enable new
	# modules/features that Purism has enabled in their defconfig. This is done
	# by: 1) generate .config using librem5_defconfig, 2) diff that .config
	# with the config in this package, 3) carefully going over the diff and
	# selecting new things enabled in their config.
	#
	# This can be uncommented to help generate the defconfig using "pmbootstrap
	# build linux-purism-librem5". The build will fail (exit 1) to let you grab
	# the config before it starts building the kernel:
	#    make ARCH="$_carch" CC="${CC:-gcc}" \
	#       defconfig KBUILD_DEFCONFIG=librem5_defconfig
	#    exit 1
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"

}

sha512sums="
755a615d7a34e4a0c0c5af60518f66a902e23b2b997b63e05faeb987cd457398a3b66dde01a674a76dda3916c55c042e0be3cb6d6e2da4623c133cad75f6321e  linux-purism-librem5-6.2.8pureos1.tar.gz
d345e7f92397a79ccf0af01e02f9a1a0669aff1173e9f34e54db18853998bd23d4aa72904e6f708deb3c2f97c6495403b1f594f340d1e84b9f11d04cba81450d  config-purism-librem5.aarch64
"
